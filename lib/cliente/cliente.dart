import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

final FirebaseAuth _auth = FirebaseAuth.instance;
const String API = "https://us-central1-organizador-uni.cloudfunctions.net/api";

class Cliente {
	static final Cliente _instance = new Cliente._privateConstructor();

	static Cliente get instance {
		return _instance;
	}

	Cliente._privateConstructor() {
		//_auth.onAuthStateChanged.listen((FirebaseUser user) async {

		//});
	}
	
	/// Loguea dado su [usuario] y [password]
	///
	/// Devuelve el usuario o nulo en caso de que el usuario no exista
	Future<dynamic> login(String usuario, String password) {
		return _request(API + "/login", <String, String> {
			'usuario': usuario,
			'password': password
		}).then((dynamic response) {
			if (response['token'] != null) {
				return _auth.signInWithCustomToken(token: response['token']);
			}

			return null;
		});
	}

	/// Registra una cuenta dado su [usuario], [password], [email]
	/// 
	/// Devuelve true si lo registro
	Future<bool> registro(String usuario, String password, String email) {
		return _request(API + "/registro", {
			'usuario': usuario,
			'password': password,
			'email': email
		}).then((dynamic response) {
			return true;
		});
	}

	Future<dynamic> _request(String url, Map<String, dynamic> body) {
		return http.post(url,
			headers: {
				'Content-Type': 'application/json'
			},
			body: json.encode(body)
		).then((http.Response response) {
			return json.decode(response.body);
		});
	}
}